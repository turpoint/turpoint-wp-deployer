<?php

/**
 * Plugin Name: WP Deployer
 * Author: Turpoint
 * Author URI: https://turpoint.com
 * Description: Provides WP-CLI command to build all Yarn assets, and deploys the website via PHP Deployer.
 * Version: 1.0.1
 * Requires at least: 5.5
 * Text Domain: turpoint-wp-deployer
 */

require_once(dirname(__FILE__) . '/inc/class-wp-deployer.php');