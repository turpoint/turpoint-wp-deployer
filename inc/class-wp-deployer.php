<?php

namespace WP_Deployer\Inc;

use WP_CLI;

class WP_Deployer
{
    public $progress;
    /**
     * Constructor
     */
    public function __construct()
    {
        // Bail if WP CLI isn't installed
        if (!class_exists('WP_CLI')) {
            return;
        }

        // Register commands
        $this->register_commands();
    }

    /**
     * Register commands
     */
    public function register_commands()
    {
        WP_CLI::add_command('deployer:deploy', [$this, 'deploy']);
    }

    /**
     * Deploy
     */
    public function deploy($args, $assoc_args) 
    {
        WP_CLI::log('Switching to the theme directory to build the assets.');
        WP_CLI::log(shell_exec('cd ' . get_template_directory() . ' && yarn build:production'));
        WP_CLI::success("Assets built, let's start the deployment!");
        WP_CLI::log(shell_exec('dep deploy production'));
        WP_CLI::success('Deployment completed!');
    }
}

/**
 * Callback
 */
$wp_deployer = new WP_Deployer();