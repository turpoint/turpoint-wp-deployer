# WP Deployer

WordPress plugin to use in local environments. Provides WP-CLI command to build all Yarn assets, and deploys the website via PHP Deployer.

**This plugin is intended to use in an installation with Roots Bedrock and Roots Sage, as this is the stack that I use myself at this moment.**

## Installation via composer

Add a reference to this repository in the `repositories` section of your projects `composer.json`:

```
"repositories" : {
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.com/turpoint/turpoint-wp-deployer.git"
    },
    ...
}
```

And then require the package:

```
composer require --dev turpoint/turpoint-wp-deployer
```

## Requirements

* WP-CLI
* A working PHP Deployer configuration

## Usage

Install the plugin. Once it is enabled, you can execute the following command in the root of your WordPress installation.

```
wp deployer:deploy
```